package com.g123k.ilv.model.livedata

import androidx.lifecycle.MutableLiveData
import com.g123k.ilv.network.NetworkManager
import com.g123k.ilv.network.model.ServerResponse
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ProductLiveData(private val barcode: String) : MutableLiveData<ServerResponse>() {

    override fun onActive() {
        super.onActive()

        GlobalScope.launch {
            val res = NetworkManager.api.getProductByBarcodeAsync(barcode).await()
            postValue(res)
        }
    }
}