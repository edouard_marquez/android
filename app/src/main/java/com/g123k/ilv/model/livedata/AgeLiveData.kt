package com.g123k.ilv.model.livedata

import androidx.lifecycle.MutableLiveData
import com.g123k.ilv.view.agify.Agify
import com.g123k.ilv.view.agify.AgifyNetworkManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AgeLiveData(val name: String) : MutableLiveData<Agify>() {

    override fun onActive() {
        super.onActive()

        GlobalScope.launch {
            val res = AgifyNetworkManager.api.getAge(name,"fr").await()
            postValue(res)
        }
    }
}