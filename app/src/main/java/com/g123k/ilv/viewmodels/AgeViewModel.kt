package com.g123k.ilv.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.g123k.ilv.model.livedata.AgeLiveData

class AgeViewModel : ViewModel() {

    // Récupérer uniquement l'age de la réponse
    fun getAge(name: String) : LiveData<Int> {
        return Transformations.map(AgeLiveData(name)) { res -> res.age }
    }

}