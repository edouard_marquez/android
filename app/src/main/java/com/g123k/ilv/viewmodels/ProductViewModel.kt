package com.g123k.ilv.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.g123k.ilv.model.Product
import com.g123k.ilv.model.livedata.ProductLiveData

class ProductViewModel : ViewModel() {

    fun getProductByBarcode(barcode: String) : LiveData<Product> {
        return Transformations.map(ProductLiveData(barcode)) { res -> res.toProduct() }
    }

}