package com.g123k.ilv.view.agify

import com.google.gson.annotations.SerializedName
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

data class Agify(
    val name: String,
    val age: Int,
    @SerializedName("country_id")
    val countryId: String,
)

interface AgifyAPI {
    @GET("/")
    fun getAge(
        @Query("name") name: String,
        @Query("country_id") countryId: String
    ): Deferred<Agify>
}

object AgifyNetworkManager {

    val api = Retrofit.Builder()
        .baseUrl("https://api.agify.io/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
        .create(AgifyAPI::class.java)

}



