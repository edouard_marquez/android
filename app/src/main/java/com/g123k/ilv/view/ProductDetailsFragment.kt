package com.g123k.ilv.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.g123k.ilv.R
import com.g123k.ilv.network.NetworkManager
import kotlinx.android.synthetic.main.sample_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProductDetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sample_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val barcode = ProductDetailsFragmentArgs.fromBundle(requireArguments()).barcode

        details_progressbar.visibility = View.VISIBLE

        GlobalScope.launch {
            val networkProduct = NetworkManager.api.getProductByBarcodeAsync(barcode).await()
            val product = networkProduct.toProduct()

            withContext(Dispatchers.Main) {
                details_progressbar.visibility = View.GONE

                details_product.visibility = View.VISIBLE
                details_product.text = product.name
            }
        }
    }

}