package com.g123k.ilv.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.g123k.ilv.R
import kotlinx.android.synthetic.main.list.*
import kotlinx.android.synthetic.main.list_item.view.*

@Suppress("DEPRECATION")
class ProductsListFragment : Fragment() {

    companion object {
        const val REQUEST_BARCODE = 100
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Données = des entiers de 1 à 100
        val numbers = (1..100).map { it.toString() }

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )

        recyclerView.adapter = ListAdapter(numbers, object : ProductClickListener {

            override fun onItemClicked(amount: Float) {

                findNavController().navigate(
                    ProductsListFragmentDirections.actionProductsListFragmentToSampleFragment("5000159484695")
                )

            }
        }
        )

        btn_scan.setOnClickListener {
            startActivityForResult(
                Intent().apply {
                action = "com.google.zxing.client.android.SCAN"
                putExtra("SCAN_FORMATS", "EAN_13")
            }, REQUEST_BARCODE
            )
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_BARCODE && data != null) {
            val barcodeFormat = data.getStringExtra("SCAN_RESULT_FORMAT")
            val barcode = data.getStringExtra("SCAN_RESULT")

            findNavController().navigate(
                ProductsListFragmentDirections.actionProductsListFragmentToSampleFragment(barcode!!)
            )
        }
    }

}

class ListAdapter(private val data: List<String>, private val listener: ProductClickListener) :
    RecyclerView.Adapter<ListCell>() {

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListCell {
        return ListCell(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ListCell, position: Int) {
        val number = data[position]
        holder.line1.text = number

        holder.itemView.setOnClickListener {
            // Ouvrir l'écran de détails
            listener.onItemClicked(10f)
        }
    }

}

class ListCell(v: View) : RecyclerView.ViewHolder(v) {

    val line1: TextView = v.cell_line1

}

interface ProductClickListener {

    // TODO A remplacer par un Product
    fun onItemClicked(amount: Float)

}